# Didasharing-tools

This is a collection of dockerfiles for teaching tools you could upload to you own server. 

From la Digitale:
- Digipad (not working) 
- Digisteps 
- Digilink 
- Digiquiz 
Others:
- gitea (self hosting git) 
- etherpad-lite (for digipad) 

Reverse proxy used: traefik

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
